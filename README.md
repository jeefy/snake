# Gluttonous Snake Game

A simple game made by **SDL**<small>(simple directmedia layer)<small>  
But here achieve it by three ways: `c`, `c++`, and `golang`

## Notice
The Full game see the codes under cpp  
c and go just made the main functions such as moving the snake and eating the food to grow length  
But cpp version made extra achievements like counting the score and showing the state

## Build

### C C++
make sure you have install SDL on your computer and you can use it with `pkg-config`  
Also, all the version need SDL\_ttf, make sure it's installed, too.  
Then, use `make` to compile `c` and `cpp` versions.  
*Notice*: *The default compiler is gcc, you can download mingw to compile this*

### Golang
This is special to build  
*for some reason, it still use `make`.*  
#### Copy Libraries
make four new directory (*pkg*, *bin*, *include* and *lib*) under go folder.  
get to the path you install the sdl and copy the `.so` (on Linux/Unix) or `.dll`(Windows) or even `.dylib`(MacOS) files under lib folder to the `lib` folder you have made.  
Rename **SDL2** to **SDLg** (all c-share library)
#### Copy Headers
copy all files under include folder to `include` folder you have made.  
Remember to rename **SDL.h** to **SDLg.h**
#### to build
Make sure you can use `go` compiler.  
Download go on [GoLang](https://golang.google.cn/dl) or [Org Site](https://golang.org/dl)  
use makefile to build everything (Do not mind command `echo` not found!)
