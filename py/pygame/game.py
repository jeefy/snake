import math
from time import sleep
from random import randint as rd

import pygame as pg

X, Y = 0, 1

WIDTH  = 800
HEIGHT = 450

SQRL = 15
FPS  = 15

BLACK = (0, 0, 0)
WHITE = pg.Color('white') #(255, 255, 255, 255)
RED = (255, 0, 0)

UP      = 0
DOWN    = 1
LEFT    = 2
RIGHT   = 3

snake = []
food = ()
direct = rd(0, 3)
failed = stopped = False
score = 0

# init for sdl
pg.init()

# init for sdl-ttf
pg.font.init()

screen = pg.display.set_mode((WIDTH, HEIGHT)) # create window
#surface = screen.get_surface()                # get surface
pg.display.set_caption("Snake")               # set title

clock = pg.time.Clock()

screen.fill(BLACK)

def init_snake():
    global snake
    snake = [(WIDTH/2, HEIGHT/2)]
    print(snake)

def new_food():
    global food
    food = (rd(0, WIDTH//SQRL - 2) * SQRL + WIDTH // 2 % SQRL, rd(0, HEIGHT//SQRL - 2) * SQRL + HEIGHT // 2 % SQRL)
    #print("food", food)

def next_snake():
    global snake, failed
    #print(snake)
    fp = snake[-1] # head
    np = list(fp) # next node
    if direct == UP:
        # remove tail and add head
        np[Y] -= SQRL
    elif direct == DOWN:
        np[Y] += SQRL
    elif direct == LEFT:
        np[X] -= SQRL
    elif direct == RIGHT:
        np[X] += SQRL

    if not 0 <= np[X] <= WIDTH or not 0 <= np[Y] <= HEIGHT:
        failed = True

    if stopped:
        return np

    np = tuple(np)
    #print(fp, np)
    snake.append(np)

    if not np == food:
        bp = snake.pop(0)
        pg.draw.rect(screen, BLACK, (bp[X], bp[Y], SQRL, SQRL))
    else:
        global score
        score += 1
        new_food()

    return np

def fail():
    global failed, score
    failed = False
    failf = pg.font.Font(None, 36).render("You Fail", True, RED) # 48px
    screen.fill(BLACK)
    screen.blit(failf, (WIDTH/2-100, HEIGHT/2-24))

    pg.display.flip()
    pg.time.delay(2000)

    screen.fill(BLACK)
    init_snake()
    new_food()
    score = 0
    return

def draw_snake(np):
    global failed
    #print('draw snake, node %d' % len(snake))
    pos = snake[-1]
    pg.draw.rect(screen, WHITE, (pos[0], pos[1], SQRL, SQRL))
    for pos in snake[:-1]:
        if pos == np:
            failed = True
        pg.draw.rect(screen, WHITE, (pos[0], pos[1], SQRL, SQRL))
    pg.draw.rect(screen, RED, (food[0], food[1], SQRL, SQRL))
    pg.display.update()

def show():
    font = pg.font.Font(None, 36)
    pg.draw.rect(screen, BLACK, (10, 10, 150, 36))
    if stopped:
        screen.blit(font.render("Stopped", True, RED), (10, 10))
    else:
        screen.blit(font.render("Score:%03d" % score, True, RED), (10, 10))


quit = False
init_snake()
new_food()
while not quit:
    clock.tick(FPS)

    #while(True):
    #    e = pg.event.poll()
    #    if e.type == pg.NOEVENT:
    #    break
    for e in pg.event.get():
        if e.type == pg.QUIT:
            quit = True
            break

        if e.type == pg.KEYDOWN:
            keys = pg.key.get_pressed()

            #print(keys)
            if keys[ord('w')] or keys[pg.K_UP]:
                if direct == DOWN:
                    failed = True
                direct = UP
            if keys[ord('s')] or keys[pg.K_DOWN]:
                if direct == UP:
                    failed = True
                direct = DOWN
            if keys[ord('a')] or keys[pg.K_LEFT]:
                if direct == RIGHT:
                    failed = True
                direct = LEFT
            if keys[ord('d')] or keys[pg.K_RIGHT]:
                if direct == LEFT:
                    failed = True
                direct = RIGHT
            if keys[ord(' ')]:
                stopped = not stopped
                print(stopped)
            if keys[pg.K_ESCAPE]:
                quit = True

    if failed:
        fail()
    draw_snake(next_snake())
    show()


pg.quit()
