from random import randint
import ctypes as C

import sdl2
import sdl2.sdlttf as ttf


class Pos():
    x, y = 0, 0

    def __init__(self, x = 0, y = 0):
        self.x, self.y = int(x), int(y)

    def toRect(self, l):
        l = int(l)
        return sdl2.SDL_Rect(int(self.x), int(self.y), l, l)

    def __str__(self):
        return "%d %d" % (self.x, self.y)

    def __eq__(self, o):
        return self.x == o.x and self.y == o.y

SQRL = 12 # snake body width

width, height = 800, 450
win = ren = sur = None

food = Pos()
stop = False

UP, DOWN, LEFT, RIGHT, direct = 0, 1, 2, 3, randint(0, 3)

snake = [] # [[x, y], [x, y]]


class Font:
    ft = None
    def __init__(self, ftpath = None, size = 0):
        if ftpath:
            self.ft = ttf.TTF_OpenFont(ftpath.encode(), size)

    __call__ = __init__

    def renderText(self, text):
        return ttf.TTF_RenderText_Blended(self.ft, text.encode(), sdl2.SDL_Color(255, 0, 0, 0))

#scrFont = Font()
failFont = Font()

def init():
    global win, ren, sur
    sdl2.SDL_Init(sdl2.SDL_INIT_VIDEO)
    ttf.TTF_Init()
    win = sdl2.SDL_CreateWindow(b"Snake", 
            sdl2.SDL_WINDOWPOS_CENTERED, sdl2.SDL_WINDOWPOS_CENTERED,
            width, height, sdl2.SDL_WINDOW_SHOWN)

    ren = sdl2.SDL_CreateRenderer(win, -1, sdl2.SDL_RENDERER_PRESENTVSYNC)
    sur = sdl2.SDL_GetWindowSurface(win)
    failFont("font.ttf", 36)

def fail():
    sdl2.SDL_RenderClear(ren)
    sdl2.SDL_FillRect(sur, None, 0)

    wsur = failFont.renderText('You Failed!')
    sdl2.SDL_BlitSurface(wsur, None, sur, sdl2.SDL_Rect(int(width/2 - 100), int(height/2 - 24)))

    update()
    sdl2.SDL_Delay(2000)
    sdl2.SDL_FillRect(sur, None, 0)
    new_food()
    init_snake()

def update():
    sdl2.SDL_RenderClear(ren)
    text = sdl2.SDL_CreateTextureFromSurface(ren, sur)
    sdl2.SDL_RenderCopy(ren, text, None, None)
    sdl2.SDL_DestroyTexture(text)
    sdl2.SDL_RenderPresent(ren)

def quit():
    sdl2.SDL_DestroyRenderer(ren)
    sdl2.SDL_DestroyWindow(win)

def init_snake():
    global snake
    snake = [Pos(width/2, height/2)]

def new_food():
    food.x = int( width / 2 + randint(-width / 2 // 12 + 1, width / 2 // 12 - 1) * 12)
    food.y = int(height / 2 + randint(-height/ 2 // 12 + 1,height / 2 // 12 - 1) * 12)


def draw_snake():
    global snake
    
    if not stop:
        # next snake
        fp = snake[-1] # head
        bp = snake[0]  # tail
        np = Pos(fp.x, fp.y) # next head
        if direct == UP:
            np.y -= SQRL
        elif direct == DOWN:
            np.y += SQRL
        elif direct == LEFT:
            np.x -= SQRL
        elif direct == RIGHT:
            np.x += SQRL

        snake.append(np)

        if not 0 <= np.x <= width or not 0 <= np.y <= height:
            fail()
            return

        for i in snake[:-1]:
            if i == np:
                fail()
                return

    # draw snake
    for p in snake:
        sdl2.SDL_FillRect(sur, p.toRect(SQRL), 0xffffffff)
        # 0xffffffff -> r 0xff, g 0xff, b 0xff, a 0xff
        # r 255, g 255, b 255, a 255

    if not stop:
        if not fp == food:
            snake.pop(0)
            sdl2.SDL_FillRect(sur, bp.toRect(SQRL), 0)
            #print(bp, food)
        else:
            new_food()

    sdl2.SDL_FillRect(sur, food.toRect(SQRL), 0xff0000)

def main():
    global direct, stop
    init()

    init_snake()
    new_food()

    e = sdl2.SDL_Event()
    run = True
    ot = sdl2.SDL_GetTicks()
    while run:
        while sdl2.SDL_PollEvent(C.byref(e)):
            if e.type == sdl2.SDL_QUIT:
                run = False
                break
            elif e.type == sdl2.SDL_KEYDOWN:
                k = e.key.keysym.sym
                if k in (sdl2.SDLK_w, sdl2.SDLK_UP):
                    if direct == DOWN:
                        fail()
                    direct = UP
                elif k in (sdl2.SDLK_s, sdl2.SDLK_DOWN):
                    if direct == UP:
                        fail()
                    direct = DOWN
                elif k in (sdl2.SDLK_a, sdl2.SDLK_LEFT):
                    if direct == RIGHT:
                        fail()
                    direct = LEFT
                elif k in (sdl2.SDLK_d, sdl2.SDLK_RIGHT):
                    if direct == LEFT:
                        fail()
                    direct = RIGHT
                elif k == sdl2.SDLK_ESCAPE:
                    run = False
                elif k == sdl2.SDLK_SPACE:
                    stop = not stop
        nt = sdl2.SDL_GetTicks()
        if 75 < abs(ot - nt):
            ot = nt
            draw_snake()
            update()

        sdl2.SDL_Delay(20)

    quit()

if __name__ == '__main__':
    main()
