#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

// no use in dev-c++ 
#ifdef __WIN32
	#pragma comment( linker, "/subsystem:windows /entry:mainCRTStartup")
#endif

typedef struct Position{
	int x, y;
} Position;

const int width = 800;
const int height = 450;

const int FPS = 16;

const int sqrl =15;

#define UP -1
#define DOWN 1
#define LEFT -2
#define RIGHT 2

int direct = LEFT;
int stop = 0;

SDL_Window * win;
SDL_Renderer * ren;
SDL_Surface * sur;

int add = 0;
int length = 0;
int failed = 0;
Position * snake;
Position food;

int randn(int n) {
	srand((unsigned)time(NULL));
	return rand() % n;
}

int cmp_pos(Position p1, Position p2) {
	return p1.x == p2.x && p1.y == p2.y;
}

void new_food() {
	food.x = randn(width / sqrl - 1) * sqrl + width / 2 % sqrl;
	food.y = randn(height/ sqrl - 1) * sqrl + height/ 2 % sqrl;
}

void rfp(SDL_Rect * rect, Position pos) { // rect update from position
	rect->x = pos.x;
	rect->y = pos.y;
}

int check(int result) {
	if (result) {
		fprintf(stderr, "%s\n", SDL_GetError());
		return 1;
	}
	return 0;
}

int check_p(void * pointer) {
	if (!pointer) {
		fprintf(stderr, "%s\n", SDL_GetError());
		return 1;
	}
	return 0;
}

void fillRect(SDL_Rect * rect, Uint32 color) {
	if (check(SDL_FillRect(sur, rect, color))) exit(1);
}

void updateWindow() {
	SDL_Texture * text = SDL_CreateTextureFromSurface(ren, sur);
	if (check_p(win)) exit(1);
	SDL_RenderClear(ren);
	if (check(SDL_RenderCopy(ren, text, NULL, NULL))) exit(1);
	SDL_DestroyTexture(text);
	SDL_RenderPresent(ren);
}

void fail() {
	SDL_RenderClear(ren);
	fillRect(NULL, 0);

	TTF_Font * ft = TTF_OpenFont("font.ttf", 36); // 48px
	if (check_p(ft)) exit(1);

	SDL_Color cl = {255, 255, 255};

	SDL_Surface * s = TTF_RenderUTF8_Blended(ft, "You Fail!", cl);
	TTF_CloseFont(ft);

	SDL_Rect r = {width / 2 - 100, height / 2 - 24, 0, 0};
	SDL_BlitSurface(s, NULL, sur, &r);
	SDL_FreeSurface(s);

	updateWindow();

	SDL_Delay(2000);

	// Reset Data
	free(snake);
	snake = (Position *)malloc(sizeof(Position) * 1);
	length = 1;
	snake[0].x = width / 2; snake[0].y = height / 2;
	new_food();
	failed = 0;
	add = 0;
	direct = LEFT;
}

void show_snake() {
	Position fp = snake[length - 1];
	Position bp = snake[0];
	
	Position np = {fp.x, fp.y};
	
	SDL_Rect r = {0, 0, sqrl, sqrl};
	

	if (!stop) {
		switch (direct) {
			case UP:
				np.y -= sqrl; break;
			case DOWN:
				np.y += sqrl; break;
			case LEFT:
				np.x -= sqrl; break;
			case RIGHT:
				np.x += sqrl; break;
		}
	}
	if (np.x < 0 || np.x > width || np.y < 0 || np.y > height) {
		failed = 1;
		return;
	}
	for (int i = 0; i < length; i++) {
		if (cmp_pos(snake[i], np)) {
			failed = 1;
			return;
		}
	}
	rfp(&r, np);
	fillRect(&r, SDL_MapRGB(sur->format, 255, 255, 255));
	
	if (!cmp_pos(np, food)) {
		length = length;
	} else {
		length++;
		new_food();
		add = 1;
	}
	
	// for new snake position
	if (!stop) {
		if (length == 1) {
			snake[0] = np;
		} else if (add) {
			// for the ram not free, i may try this new way
			// Later i found it's all cause by the updateWindow doesn't destroy texture in a proper way
			/*
			Position * temp = (Position *)malloc(sizeof(Position) * length);

			// memcpy(temp, snake + 1, sizeof(Position) * (length - 1));
			for (int i = 1; i < length; i++) {
				snake[i] = temp[i - 1];
			}

			free(snake);
			snake = temp;
			snake[length - 1] = np;
			*/
			snake = (Position *)realloc(snake, sizeof(Position) * length);
			//for (int i = length - 1; i >= 0; i--) {
			//	snake[i] = snake[i - 1];
			//}
			snake[length - 1] = np;
		} else {
			for (int i = 0; i < length; i++) {
				snake[i] = snake[i + 1];
			}
			snake[length - 1] = np;
		}
	}
	
	// clear last rect
	if (!stop || add) {
		add = 0;
		rfp(&r, bp);
		fillRect(&r, 0);
	}

	rfp(&r, food);
	fillRect(&r, SDL_MapRGB(sur->format, 255, 0, 0));
}

Uint32 timer_func(Uint32 interval, void * param) {
	if (failed) {
		fail();
		return interval;
	}
	show_snake();
	updateWindow();
	return interval;
}

int main() {
	if (check(SDL_Init(SDL_INIT_VIDEO))) exit(1);
	atexit(SDL_Quit);
	if (check(TTF_Init())) exit(1);
	atexit(TTF_Quit);
	
	win = SDL_CreateWindow("Snake", // title
							SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, // x, y
							width, height, // w, h
							SDL_WINDOW_SHOWN); // flags
	if (check_p(win)) exit(1);
	ren = SDL_CreateRenderer(win,-1,SDL_RENDERER_PRESENTVSYNC);
	if (check_p(ren)) exit(1);
	sur = SDL_GetWindowSurface(win);
	if (check_p(sur)) exit(1);

	SDL_Event e;
	
	snake = (Position *)malloc(sizeof(Position) * 1);
	length = 1;
	snake[0].x = width / 2; snake[0].y = height / 2;
	new_food();
	
	SDL_TimerID timerid = SDL_AddTimer(1000 / FPS, timer_func, NULL);
	
	while (1) {
		while (SDL_PollEvent(&e)) {
			switch (e.type) {
				case SDL_QUIT:
					goto __quit;
				case SDL_KEYDOWN:
					switch (e.key.keysym.sym) {
						case 'w':
						case SDLK_UP:
							direct = UP; break;
						case SDLK_DOWN:
						case 's':
							direct = DOWN; break;
						case SDLK_LEFT:
						case 'a':
							direct = LEFT; break;
						case 'd':
						case SDLK_RIGHT:
							direct = RIGHT; break;
						case SDLK_ESCAPE:
							goto __quit;
						case ' ':
							stop = !stop; break;
					}
			}
		}
		SDL_Delay(20);
	}
	

__quit:
	SDL_RemoveTimer(timerid);
	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);
	return 0;
}

int WinMain() {
	return main();
}
