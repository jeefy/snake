#ifndef _SNAKE_H_
#define _SNAKE_H_

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

// Window's size
const int width = 800;
const int height = 450;

const int UP = -1;
const int DOWN = 1;
const int LEFT = -2;
const int RIGHT = 2;

const int sqrl = 12;

int direct = -2;
bool stop = false;
bool failed = false;

int randn(int);

void fail();

Uint32 BLACK = 0;
Uint32 WHITE = 0;
Uint32 RED = 0;
void init_color();

class Winrs {
public:
	Winrs();
	SDL_Window * win;
	SDL_Renderer * ren;
	SDL_Surface * sur;

	int fillRect(SDL_Rect*, Uint32 color);
	int clearRect(SDL_Rect*);
	int clearR(); // clear Renderer (FULL)
	int applySurface(SDL_Surface*, SDL_Rect*);
	int applyTexture(SDL_Texture*, SDL_Rect*);
	int update();
	SDL_Texture * createTextureFromSurface(SDL_Surface*);

	void quit();
};

class Event {
public:
	SDL_Event e;
	Uint32 type;
	int Poll();
	int checkClick(SDL_Rect);
	Uint32 key();
};

typedef struct {
	int x;
	int y;
} Position;

class Snake {
public:
	Snake();
	Position * pos;
	Position food_pos;
	Position bp;
	int add;
	int length;
	void next();
	void draw();
	void check();
	void new_food();

	void reset();

	const static int pos_s = sizeof(Position);
};

class Score {
public:
	Score();
	Position spos; // show position
	Position npos; // number position
	SDL_Color red;
	SDL_Surface * scr;
	SDL_Surface * stp;
	SDL_Surface * nbr;
	TTF_Font * font;
	int scores;
	bool changed;
	void show();
	void add();
	void reset();

	void quit();
};

Winrs * winrs;
Snake * snake;
Score * score;

int check(int);
int check_p(void *);
int cmp_pos(Position, Position);
#endif // _SNAKE_H_
