#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <time.h>

#include <snake.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

using std::cout;
#define endl '\n'

int check(int result) {
	if (result != 0) {
		fprintf(stderr, "Snake Meet Some Error: %s\n", SDL_GetError());
		return 1;
	}
	return 0;
}

int check_p(void *pointer) {
	return check(pointer == NULL ? 1 : 0);
}

int cmp_pos(Position p1, Position p2) {
	return p1.x == p2.x && p1.y == p2.y;
}

void rfp(SDL_Rect * rect, Position p) {
	rect->x = p.x;
	rect->y = p.y;
}

int randn(int n) {
	srand((unsigned)time(NULL));
	return rand() % n;
}

Winrs::Winrs() {
	win = SDL_CreateWindow("Snake", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
			width, height, SDL_WINDOW_SHOWN);
	if (check_p(win)) exit(1);

	ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_PRESENTVSYNC);
	if (check_p(win)) exit(1);

	sur = SDL_GetWindowSurface(win);
	if (check_p(sur)) exit(1);
}

int Winrs::fillRect(SDL_Rect * rect, Uint32 color) {
	if (check(SDL_FillRect(sur, rect, color))) exit(1);
	return 0;
}

int Winrs::clearRect(SDL_Rect * rect) {
	return fillRect(rect, BLACK);
}

int Winrs::clearR() {
	// fillRect(NULL, BLACK);
	return check(SDL_RenderClear(ren));
}

int Winrs::applySurface(SDL_Surface * surface, SDL_Rect * rect) {
	if (check(SDL_BlitSurface(surface, NULL, sur, rect))) exit(1);
	return 0;
}

int Winrs::applyTexture(SDL_Texture * text, SDL_Rect * rect) {
	if (check(SDL_RenderCopy(ren, text, NULL, rect))) exit(1);
	return 0;
}

int Winrs::update() {
	SDL_Texture * text;
	if (check_p((text = createTextureFromSurface(sur)))) exit(1);
	applyTexture(text, NULL);
	SDL_DestroyTexture(text);
	SDL_RenderPresent(ren);
	return 0;
}

void Winrs::quit() {
	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);
}

SDL_Texture * Winrs::createTextureFromSurface(SDL_Surface * text) {
	return SDL_CreateTextureFromSurface(ren, text);
}

int Event::Poll() {
	int r = SDL_PollEvent(&e);
	type = e.type;
	return r;
}

Uint32 Event::key() {
	return e.key.keysym.sym;
}

int Event::checkClick(SDL_Rect r) {
	int x = e.button.x;
	int y = e.button.y;

	if (r.x <= x && x <= r.x + r.w && r.y <= y && y <= r.y + r.h) {
		return 1;
	}
	return 0;
}

Snake::Snake() {
	pos = (Position *)malloc(pos_s);
	length = 1;
	add = 0;
	pos[0].x = width / 2;
	pos[0].y = height / 2;
	// cout << pos[0].x << ' ' << pos[0].y << endl;
	new_food();
}

void Snake::reset() {
	free(pos);
	pos = (Position *)malloc(pos_s);
	length = 1;
	add = 0;
	pos[0].x = width / 2;
	pos[0].y = height / 2;
	// cout << pos[0].x << ' ' << pos[0].y << endl;
	new_food();
}

void Snake::new_food() {
	food_pos.x = randn(width / sqrl - 1) * sqrl + width / 2 % sqrl;
	food_pos.y = randn(height/ sqrl - 1) * sqrl + height / 2 % sqrl;
	// cout << food_pos.x << ' ' << food_pos.y << endl;
}

void Snake::next() {
	if (stop) return;
	Position hp = pos[length - 1];
	bp = pos[0];
	if (cmp_pos(food_pos, hp)) {
		new_food();
		length++;
		pos = (Position *)realloc(pos, pos_s * length);
		add = 1;
		score->add();
	} else {
		Position * posb = (Position *)malloc(pos_s * length);
		memcpy(posb, pos + 1, sizeof(Position) * (length - 1));
		free(pos);
		pos = posb;
	}

	Position np = {hp.x, hp.y};
	switch (direct) {
		case UP:
			np.y -= sqrl; break;
		case DOWN:
			np.y += sqrl; break;
		case LEFT:
			np.x -= sqrl; break;
		case RIGHT:
			np.x += sqrl; break;
	}

	pos[length - 1] = np;
}

void Snake::draw() {
	SDL_Rect r = {0, 0, sqrl, sqrl};
	rfp(&r, food_pos);
	winrs->fillRect(&r, RED);

	/*
	for (int i = 0; i < length; i++) {
		rfp(&r, pos[i]);
		winrs->fillRect(&r, WHITE);
	}
	*/
	rfp(&r, pos[length - 1]);
	winrs->fillRect(&r, WHITE);

	if (!add) {
		rfp(&r, bp);
		winrs->fillRect(&r, BLACK);
	}
	add = 0;
}

void Snake::check() {
	Position fp = pos[length - 1];
	if (fp.x <= 0 || fp.x >= width || fp.y <= 0 || fp.y >= height) {
		failed = true;
		return;
	}

	for (int i = 0; i < length - 4; i++) {
		if (cmp_pos(fp, pos[i])) {
			failed = true;
			return;
		}
	}
}

Score::Score() {
	font = TTF_OpenFont("font.ttf", 18); // 24px
	if (check_p(font)) exit(1);

	red.r = 255; red.g = 0; red.b = 0; red.a = 0;
	stp = TTF_RenderText_Blended(font, "Stopped", red);
	scr = TTF_RenderText_Blended(font, "Score:", red);

	spos.x = 5; spos.y = 5;
	npos.x = 60; npos.y = 5;
	changed = true;

	scores = 0;
}

void Score::quit() {
	TTF_CloseFont(font);

	SDL_FreeSurface(stp);
	SDL_FreeSurface(scr);
	// SDL_FreeSurface(nbr);
}

void Score::show() {
	SDL_Rect rect = {0, 0, 150, 24};
	rfp(&rect, spos);
	if (stop) {
		winrs->clearRect(&rect);
		winrs->applySurface(stp, &rect);
		return;
	}
	winrs->clearRect(&rect);
	winrs->applySurface(scr, &rect);

	rfp(&rect, npos);
	if (!changed) {
		// winrs->clearRect(&rect);
		winrs->applySurface(nbr, &rect);
		return;
	}

	char scrs[4];
	sprintf(scrs, "%03d", scores);

	static int free = 0;
	if (!free) {
		free = 1;
	} else {
		SDL_FreeSurface(nbr);
	}
	nbr = TTF_RenderText_Blended(font, scrs, red);
	// winrs->clearRect(&rect);
	winrs->applySurface(nbr, &rect);


	changed = false;
}

void Score::add() {
	scores += 1;
	changed = true;
}

void Score::reset() {
	scores = 0;
	changed = true;
}

void init_color() {
	WHITE = SDL_MapRGB(winrs->sur->format, 255, 255, 255);
	RED = SDL_MapRGB(winrs->sur->format, 255, 0, 0);
}

Uint32 update_win(Uint32 interval, void * param) {
	if (failed) {
		fail();
		return interval;
	}
	winrs->clearR();
	score->show();
	snake->next();
	snake->check();
	snake->draw();
	winrs->update();
	return interval;
}

void fail() {
	cout << "You Fail" << endl;
	TTF_Font * font = TTF_OpenFont("font.ttf", 36); // 48px
	if (check_p(font)) exit(1);

	winrs->clearR();
	winrs->clearRect(NULL);

	SDL_Color cl = {255, 255, 255, 0};
	SDL_Surface * sur = TTF_RenderText_Blended(font, "You Fail!", cl);

	SDL_Rect sp = {width / 2 - 100, height / 2 - 24, 0, 0};
	winrs->applySurface(sur, &sp);

	SDL_FreeSurface(sur);

	winrs->update();

	SDL_Delay(2000);

	winrs->clearRect(NULL);

	TTF_CloseFont(font);

	failed = false;
	snake->reset();
	score->reset();
}

int main() {
	if (check(SDL_Init(SDL_INIT_VIDEO|SDL_INIT_EVENTS|SDL_INIT_TIMER))) exit(1);
	if (check(TTF_Init())) exit(1);

	winrs = new Winrs();
	snake = new Snake();
	score = new Score();

	init_color();

	SDL_TimerID id = SDL_AddTimer(1000 / 16, update_win, NULL);

	Event e;

	while (1) {
		while (e.Poll()) {
			switch (e.type) {
				case SDL_QUIT:
					goto __quit;
				case SDL_KEYDOWN:
					switch (e.key()) {
						case 'w':
						case SDLK_UP:
							if (direct == -UP) failed = true;
							direct = UP; break;
						case 's':
						case SDLK_DOWN:
							if (direct == UP) failed = true;
							direct = DOWN; break;
						case 'a':
						case SDLK_LEFT:
							if (direct == -LEFT) failed = true;
							direct = LEFT; break;
						case 'd':
						case SDLK_RIGHT:
							if (direct == LEFT) failed = true;
							direct = RIGHT; break;
						case ' ':
							stop = !stop; break;
						case SDLK_ESCAPE:
							goto __quit;
					}
					break;
					
			}	
		}
	}

__quit:
	SDL_RemoveTimer(id);
	winrs->quit();
	score->quit();
	TTF_Quit();
	delete winrs;
	delete snake;
	delete score;
}
